# Brazilian Portuguese translation for ubuntu-calendar-app
# Copyright (c) 2013 Rosetta Contributors and Canonical Ltd 2013
# This file is distributed under the same license as the ubuntu-calendar-app package.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2013.
#
msgid ""
msgstr ""
"Project-Id-Version: ubuntu-calendar-app\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-03-07 08:29+0000\n"
"PO-Revision-Date: 2019-04-25 19:14+0000\n"
"Last-Translator: Vinícius F <magnetuz@protonmail.com>\n"
"Language-Team: Portuguese (Brazil) <https://translate.ubports.com/projects/"
"ubports/calendar-app/pt_BR/>\n"
"Language: pt_BR\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n > 1;\n"
"X-Generator: Weblate 3.2.2\n"
"X-Launchpad-Export-Date: 2017-04-05 07:14+0000\n"

#: ../qml/TimeLineBase.qml:50 ../qml/AllDayEventComponent.qml:89
msgid "New event"
msgstr "Novo evento"

#: ../qml/CalendarChoicePopup.qml:44 ../qml/EventActions.qml:54
msgid "Calendars"
msgstr "Calendários"

#: ../qml/CalendarChoicePopup.qml:46 ../qml/SettingsPage.qml:51
msgid "Back"
msgstr "Voltar"

#. TRANSLATORS: Please translate this string  to 15 characters only.
#. Currently ,there is no way we can increase width of action menu currently.
#: ../qml/CalendarChoicePopup.qml:58 ../qml/EventActions.qml:39
msgid "Sync"
msgstr "Sincronizar"

#: ../qml/CalendarChoicePopup.qml:58 ../qml/EventActions.qml:39
msgid "Syncing"
msgstr "Sincronizando"

#: ../qml/CalendarChoicePopup.qml:83
msgid "Add online Calendar"
msgstr "Adicionar calendário online"

#: ../qml/CalendarChoicePopup.qml:184
msgid "Unable to deselect"
msgstr "Não é possível desmarcar"

#: ../qml/CalendarChoicePopup.qml:185
msgid ""
"In order to create new events you must have at least one writable calendar "
"selected"
msgstr ""
"Para criar novos eventos, você deve ter pelo menos um calendário gravável "
"selecionado"

#: ../qml/CalendarChoicePopup.qml:187 ../qml/RemindersPage.qml:80
msgid "Ok"
msgstr "Ok"

#. TRANSLATORS: the first argument (%1) refers to a start time for an event,
#. while the second one (%2) refers to the end time
#: ../qml/EventBubble.qml:139
msgid "%1 - %2"
msgstr "%1 - %2"

#: ../qml/RemindersModel.qml:31 ../qml/RemindersModel.qml:99
msgid "No Reminder"
msgstr "Não lembrar"

#. TRANSLATORS: this refers to when a reminder should be shown as a notification
#. in the indicators. "On Event" means that it will be shown right at the time
#. the event starts, not any time before
#: ../qml/RemindersModel.qml:34 ../qml/RemindersModel.qml:103
msgid "On Event"
msgstr "No evento"

#: ../qml/RemindersModel.qml:43
msgid "%1 week"
msgid_plural "%1 weeks"
msgstr[0] "%1 semana"
msgstr[1] "%1 semanas"

#: ../qml/RemindersModel.qml:54
msgid "%1 day"
msgid_plural "%1 days"
msgstr[0] "%1 dia"
msgstr[1] "%1 dias"

#: ../qml/RemindersModel.qml:65
msgid "%1 hour"
msgid_plural "%1 hours"
msgstr[0] "%1 hora"
msgstr[1] "%1 horas"

#: ../qml/RemindersModel.qml:74
msgid "%1 minute"
msgid_plural "%1 minutes"
msgstr[0] "%1 minuto"
msgstr[1] "%1 minutos"

#: ../qml/RemindersModel.qml:104
msgid "5 minutes"
msgstr "5 minutos"

#: ../qml/RemindersModel.qml:105
msgid "10 minutes"
msgstr "10 minutos"

#: ../qml/RemindersModel.qml:106
msgid "15 minutes"
msgstr "15 minutos"

#: ../qml/RemindersModel.qml:107
msgid "30 minutes"
msgstr "30 minutos"

#: ../qml/RemindersModel.qml:108
msgid "1 hour"
msgstr "1 hora"

#: ../qml/RemindersModel.qml:109
msgid "2 hours"
msgstr "2 horas"

#: ../qml/RemindersModel.qml:110
msgid "1 day"
msgstr "1 dia"

#: ../qml/RemindersModel.qml:111
msgid "2 days"
msgstr "2 dias"

#: ../qml/RemindersModel.qml:112
msgid "1 week"
msgstr "1 semana"

#: ../qml/RemindersModel.qml:113
msgid "2 weeks"
msgstr "2 semanas"

#: ../qml/RemindersModel.qml:114
msgid "Custom"
msgstr "Personalizado"

#: ../qml/AgendaView.qml:50 ../qml/calendar.qml:333 ../qml/calendar.qml:514
msgid "Agenda"
msgstr "Agenda"

#: ../qml/AgendaView.qml:95
msgid "You have no calendars enabled"
msgstr "Você não tem calendários habilitados"

#: ../qml/AgendaView.qml:95
msgid "No upcoming events"
msgstr "Não há eventos programados"

#: ../qml/AgendaView.qml:107
msgid "Enable calendars"
msgstr "Habilitar calendários"

#: ../qml/AgendaView.qml:199
msgid "no event name set"
msgstr "nenhum nome definido para o evento"

#: ../qml/AgendaView.qml:201
msgid "no location"
msgstr "sem localização"

#: ../qml/LimitLabelModel.qml:25
msgid "Never"
msgstr "Nunca"

#: ../qml/LimitLabelModel.qml:26
msgid "After X Occurrence"
msgstr "Após X Ocorrências"

#: ../qml/LimitLabelModel.qml:27
msgid "After Date"
msgstr "Após a data"

#: ../qml/NewEventBottomEdge.qml:54 ../qml/NewEvent.qml:382
msgid "New Event"
msgstr "Novo evento"

#. TRANSLATORS: Please keep the translation of this string to a max of
#. 5 characters as the week view where it is shown has limited space.
#: ../qml/AllDayEventComponent.qml:148
msgid "%1 event"
msgid_plural "%1 events"
msgstr[0] "%1 evento"
msgstr[1] "%1 eventos"

#. TRANSLATORS: the argument refers to the number of all day events
#: ../qml/AllDayEventComponent.qml:152
msgid "%1 all day event"
msgid_plural "%1 all day events"
msgstr[0] "%1 evento de dia inteiro"
msgstr[1] "%1 eventos de dia inteiro"

#: ../qml/EditEventConfirmationDialog.qml:29 ../qml/NewEvent.qml:382
msgid "Edit Event"
msgstr "Editar evento"

#. TRANSLATORS: argument (%1) refers to an event name.
#: ../qml/EditEventConfirmationDialog.qml:32
msgid "Edit only this event \"%1\", or all events in the series?"
msgstr "Editar apenas este evento \"%1\", ou todos os eventos da série?"

#: ../qml/EditEventConfirmationDialog.qml:35
msgid "Edit series"
msgstr "Editar séries"

#: ../qml/EditEventConfirmationDialog.qml:44
msgid "Edit this"
msgstr "Editar isto"

#: ../qml/EditEventConfirmationDialog.qml:53
#: ../qml/DeleteConfirmationDialog.qml:60 ../qml/RemindersPage.qml:72
#: ../qml/NewEvent.qml:387 ../qml/OnlineAccountsHelper.qml:73
#: ../qml/ColorPickerDialog.qml:55
msgid "Cancel"
msgstr "Cancelar"

#: ../qml/ContactChoicePopup.qml:37
msgid "No contact"
msgstr "Nenhum contato"

#: ../qml/ContactChoicePopup.qml:96
msgid "Search contact"
msgstr "Pesquisar contato"

#: ../qml/DeleteConfirmationDialog.qml:31
msgid "Delete Recurring Event"
msgstr "Apagar evento atual"

#: ../qml/DeleteConfirmationDialog.qml:32
msgid "Delete Event"
msgstr "Excluir evento"

#. TRANSLATORS: argument (%1) refers to an event name.
#: ../qml/DeleteConfirmationDialog.qml:36
msgid "Delete only this event \"%1\", or all events in the series?"
msgstr "Excluir apenas o evento \"%1\", ou todos os eventos da série?"

#: ../qml/DeleteConfirmationDialog.qml:37
msgid "Are you sure you want to delete the event \"%1\"?"
msgstr "Você tem certeza que deseja excluir o evento \"%1\"?"

#: ../qml/DeleteConfirmationDialog.qml:40
msgid "Delete series"
msgstr "Excluir série"

#: ../qml/DeleteConfirmationDialog.qml:51
msgid "Delete this"
msgstr "Apagar isto"

#: ../qml/DeleteConfirmationDialog.qml:51 ../qml/NewEvent.qml:394
msgid "Delete"
msgstr "Excluir"

#: ../qml/calendar.qml:74
msgid ""
"Calendar app accept four arguments: --starttime, --endtime, --newevent and --"
"eventid. They will be managed by system. See the source for a full comment "
"about them"
msgstr ""
"O aplicativo do Calendário aceita quatro argumentos: --starttime, --endtime, "
"--newevent e --eventid. Eles serão geridos pelo sistema. Veja a fonte de um "
"comentário completo sobre eles"

#: ../qml/calendar.qml:341 ../qml/calendar.qml:535
msgid "Day"
msgstr "Dia"

#: ../qml/calendar.qml:349 ../qml/calendar.qml:556
msgid "Week"
msgstr "Semana"

#: ../qml/calendar.qml:357 ../qml/calendar.qml:577
msgid "Month"
msgstr "Mês"

#: ../qml/calendar.qml:365 ../qml/calendar.qml:598
msgid "Year"
msgstr "Ano"

#: ../qml/calendar.qml:705 ../qml/TimeLineHeader.qml:66
#: ../qml/EventDetails.qml:173
msgid "All Day"
msgstr "Dia inteiro"

#: ../qml/SettingsPage.qml:49 ../qml/EventActions.qml:66
msgid "Settings"
msgstr "Configurações"

#: ../qml/SettingsPage.qml:72
msgid "Show week numbers"
msgstr "Mostrar números da semana"

#: ../qml/SettingsPage.qml:90
msgid "Display Chinese calendar"
msgstr "Mostrar calendário chinês"

#: ../qml/SettingsPage.qml:110
msgid "Business hours"
msgstr "Horário comercial"

#: ../qml/SettingsPage.qml:211
msgid "Default reminder"
msgstr "Lembrete padrão"

#: ../qml/SettingsPage.qml:255
msgid "Default calendar"
msgstr "Calendário padrão"

#: ../qml/RemindersPage.qml:62
msgid "Custom reminder"
msgstr "Lembrete personalizado"

#. TRANSLATORS: This is shown in the month view as "Wk" as a title
#. to indicate the week numbers. It should be a max of up to 3 characters.
#: ../qml/MonthComponent.qml:293
msgid "Wk"
msgstr "Sem"

#. TRANSLATORS: this refers to how often a recurrent event repeats
#. and it is shown as the header of the page to choose repetition
#. and as the header of the list item that shows the repetition
#. summary in the page that displays the event details
#: ../qml/EventRepetition.qml:40 ../qml/EventRepetition.qml:167
msgid "Repeat"
msgstr "Repetir"

#: ../qml/EventRepetition.qml:187
msgid "Repeats On:"
msgstr "Repetição ligada:"

#: ../qml/EventRepetition.qml:233
msgid "Interval of recurrence"
msgstr "Intervalo de recorrência"

#: ../qml/EventRepetition.qml:258
msgid "Recurring event ends"
msgstr "Evento recorrente termina"

#. TRANSLATORS: this refers to how often a recurrent event repeats
#. and it is shown as the header of the option selector to choose
#. its repetition
#: ../qml/EventRepetition.qml:282 ../qml/NewEvent.qml:775
msgid "Repeats"
msgstr "Repetições"

#: ../qml/EventRepetition.qml:308
msgid "Date"
msgstr "Data"

#: ../qml/YearView.qml:57 ../qml/WeekView.qml:60 ../qml/MonthView.qml:50
#: ../qml/DayView.qml:76
msgid "Today"
msgstr "Hoje"

#: ../qml/YearView.qml:79
msgid "Year %1"
msgstr "Ano %1"

#: ../qml/NewEvent.qml:199
msgid "End time can't be before start time"
msgstr "Horário final não pode ser antes do horário inicial"

#: ../qml/NewEvent.qml:412
msgid "Save"
msgstr "Salvar"

#: ../qml/NewEvent.qml:423
msgid "Error"
msgstr "Erro"

#: ../qml/NewEvent.qml:425
msgid "OK"
msgstr "OK"

#: ../qml/NewEvent.qml:487
msgid "From"
msgstr "De"

#: ../qml/NewEvent.qml:503
msgid "To"
msgstr "Para"

#: ../qml/NewEvent.qml:530
msgid "All day event"
msgstr "Evento de dia inteiro"

#: ../qml/NewEvent.qml:553 ../qml/EventDetails.qml:37
msgid "Event Details"
msgstr "Detalhes do Evento"

#: ../qml/NewEvent.qml:567
msgid "Event Name"
msgstr "Nome do evento"

#: ../qml/NewEvent.qml:585 ../qml/EventDetails.qml:437
msgid "Description"
msgstr "Descrição"

#: ../qml/NewEvent.qml:604
msgid "Location"
msgstr "Localização"

#: ../qml/NewEvent.qml:619 ../qml/EventDetails.qml:348
#: com.ubuntu.calendar_calendar.desktop.in.h:1
msgid "Calendar"
msgstr "Calendário"

#: ../qml/NewEvent.qml:681
msgid "Guests"
msgstr "Convidados"

#: ../qml/NewEvent.qml:691
msgid "Add Guest"
msgstr "Adicionar convidado"

#: ../qml/NewEvent.qml:797 ../qml/NewEvent.qml:814 ../qml/EventDetails.qml:464
msgid "Reminder"
msgstr "Lembrete"

#: ../qml/WeekView.qml:137 ../qml/MonthView.qml:76
msgid "%1 %2"
msgstr "%1 %2"

#: ../qml/WeekView.qml:144 ../qml/WeekView.qml:145
msgid "MMM"
msgstr "MMM"

#. TRANSLATORS: this is a time formatting string,
#. see http://qt-project.org/doc/qt-5/qml-qtqml-date.html#details for valid expressions.
#. It's used in the header of the month and week views
#: ../qml/WeekView.qml:156 ../qml/MonthView.qml:81 ../qml/DayView.qml:126
msgid "MMMM yyyy"
msgstr "MMMM yyyy"

#. TRANSLATORS: W refers to Week, followed by the actual week number (%1)
#: ../qml/TimeLineHeader.qml:54
msgid "W%1"
msgstr "S%1"

#: ../qml/EventDetails.qml:40
msgid "Edit"
msgstr "Editar"

#: ../qml/EventDetails.qml:392
msgid "Attending"
msgstr "Participando"

#: ../qml/EventDetails.qml:394
msgid "Not Attending"
msgstr "Não participando"

#: ../qml/EventDetails.qml:396
msgid "Maybe"
msgstr "Talvez"

#: ../qml/EventDetails.qml:398
msgid "No Reply"
msgstr "Sem resposta"

#: ../qml/OnlineAccountsHelper.qml:39
msgid "Pick an account to create."
msgstr "Escolha uma conta para criar."

#: ../qml/ColorPickerDialog.qml:25
msgid "Select Color"
msgstr "Selecionar cor"

#: ../qml/RecurrenceLabelDefines.qml:23
msgid "Once"
msgstr "Uma vez"

#: ../qml/RecurrenceLabelDefines.qml:24
msgid "Daily"
msgstr "Diariamente"

#: ../qml/RecurrenceLabelDefines.qml:25
msgid "On Weekdays"
msgstr "Nos dias da semana"

#. TRANSLATORS: The arguments refer to days of the week. E.g. "On Monday, Tuesday, Thursday"
#: ../qml/RecurrenceLabelDefines.qml:27
msgid "On %1, %2 ,%3"
msgstr "No(a) %1, %2 ,%3"

#. TRANSLATORS: The arguments refer to days of the week. E.g. "On Monday and Thursday"
#: ../qml/RecurrenceLabelDefines.qml:29
msgid "On %1 and %2"
msgstr "No(a) %1 e %2"

#: ../qml/RecurrenceLabelDefines.qml:30
msgid "Weekly"
msgstr "Semanalmente"

#: ../qml/RecurrenceLabelDefines.qml:31
msgid "Monthly"
msgstr "Mensalmente"

#: ../qml/RecurrenceLabelDefines.qml:32
msgid "Yearly"
msgstr "Anualmente"

#. TRANSLATORS: the argument refers to multiple recurrence of event with count .
#. E.g. "Daily; 5 times."
#: ../qml/EventUtils.qml:75
msgid "%1; %2 time"
msgid_plural "%1; %2 times"
msgstr[0] "%1; %2 vez"
msgstr[1] "%1; %2 vezes"

#. TRANSLATORS: the argument refers to recurrence until user selected date.
#. E.g. "Daily; until 12/12/2014."
#: ../qml/EventUtils.qml:79
msgid "%1; until %2"
msgstr "%1; desde %2"

#: ../qml/EventUtils.qml:93
msgid "; every %1 days"
msgstr "; cada %1 dias"

#: ../qml/EventUtils.qml:95
msgid "; every %1 weeks"
msgstr "; cada %1 semanas"

#: ../qml/EventUtils.qml:97
msgid "; every %1 months"
msgstr "; cada %1 meses"

#: ../qml/EventUtils.qml:99
msgid "; every %1 years"
msgstr "; cada %1 anos"

#. TRANSLATORS: the argument refers to several different days of the week.
#. E.g. "Weekly on Mondays, Tuesdays"
#: ../qml/EventUtils.qml:125
msgid "Weekly on %1"
msgstr "Semanalmente em %1"

#: com.ubuntu.calendar_calendar.desktop.in.h:2
msgid "A calendar for Ubuntu which syncs with online accounts."
msgstr "Um calendário para Ubuntu que sincroniza com as contas online."

#: com.ubuntu.calendar_calendar.desktop.in.h:3
msgid "calendar;event;day;week;year;appointment;meeting;"
msgstr "calendário;evento;dia;semana;ano;encontro;reunião;"

#~ msgid "Show lunar calendar"
#~ msgstr "Mostrar calendário lunar"
